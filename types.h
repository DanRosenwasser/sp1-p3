
/**
 * Defines certain useful fundamental types to make some things easier.
 *
 * Author: Daniel Rosenwasser
 */

#ifndef __TYPES_H_
#define __TYPES_H_

#define size_t  unsigned int

#endif
