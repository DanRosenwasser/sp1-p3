/*
** SCCS ID:    @(#)main.c    1.3    03/15/05
**
** File:    main.c
**
** Author:    K. Reek
**
** Contributor:    Warren R. Carithers, Daniel Rosenwasser, Matthew Newton
**
** Description:    Dummy main program
*/
#include "c_io.h"
#include "x86arch.h"
#include "support.h"
#include "startup.h"
#include <uart.h>
#include "serial_io.h"
#include "utility.h"
#include "accuracy_calc.h"
#define SERIAL_BUFFER_SIZE 256
#define CLOCK_TICKS_PER_SEC 18

extern int num_sentences;
extern char *sentences[];

static int time;

inline void enable_interrupts(void)
{
    asm("sti");
}
void timer_handler(int vector, int code)
{
    ++time;
    __outb( PIC_MASTER_CMD_PORT, PIC_EOI );
}
inline void install_ISRs(void)
{
    c_printf("Installing ISRs...\n");
    __install_isr(INT_VEC_TIMER, timer_handler);
    __install_isr(INT_VEC_SERIAL_PORT_1, sio_interrupt);
}

void clear_ADDS_screen(void)
{
    sio_putchar('\x1a');
}
inline void delay(int n_seconds)
{
    time = 0;
    while(time < (n_seconds * CLOCK_TICKS_PER_SEC));
}

int main( void )
{
    install_ISRs();
    init_sio();
    
    c_printf("\n\n*Enabling ALL Interrupts*\n\n");
    enable_interrupts();
    //c_puts( "Hello World!\n" );
    clear_ADDS_screen();

    sio_puts("Daniel Rosenwasser  (dxr4911)\n");
    sio_puts("Matthew Newton      (mln6469)\n");

    sio_puts("\n");
    
    char buf[SERIAL_BUFFER_SIZE];
    
    //universal iterators
    int i, j;
    
    // length of the input per given trial, which adds up to chars_read
    unsigned int input_len;
    
    // total number of characters read per test
    unsigned int chars_read;
    
    // total number of clock ticks that have occurred per test
    unsigned int cticks;
    
    // total number of errors encountered per test
    unsigned int num_errors;
    while(1){
	    for (i = 0; i < num_sentences; i++)
	    {
	    sio_set_echo_status(0);
		chars_read = cticks = num_errors = 0;
		c_puts("\n Testing Sentence \n");
		c_puts(sentences[i]);
		c_puts("\n");
		
		// Prompt
		sio_puts("\nPress the enter key when you are ready:\n");
		
		// Block for an end-of-line - gets could cause a buffer overflow
		while (sio_getchar() != '\n');
		
		sio_puts(sentences[i]);
		sio_putchar('\n');

		//wait 1 second and send a bell
		delay(1);
		sio_putchar('\a');
		
		// wait until the transmitter flag goes on
		while (!sio_get_tx_state());
		//begin counting
		sio_set_echo_status(1);
		time = 0;

		for (j = 0; j < 3; j++)
		{
		    sio_gets(buf, SERIAL_BUFFER_SIZE);
		    input_len = strlen(buf);

		    
		    if (input_len > 1 && buf[input_len-1] == '\n')
		    {
		        buf[strlen(buf)-1] = '\0';
		        input_len -= 1;
		    }
		    
		    chars_read += input_len;
		    
		    num_errors += differences(sentences[i], buf);
		}
		
		cticks = time; // TODO: cticks redundant?
		
		sio_puts("You entered ");
		sio_print_integer(chars_read);
		sio_puts(" characters with ");
		sio_print_integer(num_errors);
		sio_puts(" errors in ");
		sio_print_integer(cticks);
		sio_puts(" clock ticks.\n");
		sio_puts("Your speed was ");
		//get seconds by rounding up clock ticks and dividing by CLOCK_TICKS_PER_SEC
		int seconds = cticks + (CLOCK_TICKS_PER_SEC - (cticks % CLOCK_TICKS_PER_SEC));
		seconds /= CLOCK_TICKS_PER_SEC;
		sio_print_integer(chars_per_second(chars_read, seconds));
		sio_putchar('.');
		sio_print_integer(tenths_chars_per_second(chars_read, seconds));
		sio_puts(" characters per second.\n");
		
		c_puts("Roger that. \n");
		c_printf("\nThis guy got %d differences in %d clock ticks",
		            num_errors, cticks);
		sio_puts("\n");
	    }
	    c_puts("Looping\n");
    }
    //while(1);
    
    return( 0 );
}

