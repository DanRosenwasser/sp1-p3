
/**
 *
 * Author: Daniel Rosenwasser, Matthew Newton
 *
 * Contributor: Walter Carithers
 *
 */

#include "serial_io.h"
#include "x86arch.h"
#include "support.h"
#include "c_io.h"
#include "startup.h"

#include <uart.h>

#define CTRL_D 0x4
#define INPUT_BUFFER_SIZE 255

static int rx_ready_flag;
static int tx_ready_flag;

static int read_enable_flag = 0;
static int echo_enable_flag = 0;

static char input_buffer[INPUT_BUFFER_SIZE+1] = {0};
static int next_char = 0;
static int buff_read = 0;


void init_sio(void)
{
    c_printf("Configuring FIFO...\n");
    __outb(UA4_BANK_SELECT, UA4_LCR_BANK0);
    __outb(UA4_FCR, 0);
    __outb(UA4_FCR, UA5_FCR_FIFO_ENABLED);
    __outb(UA4_FCR, UA5_FCR_FIFO_ENABLED|UA5_FCR_RX_SOFT_RESET);
    __outb(UA4_FCR, UA5_FCR_FIFO_ENABLED|UA5_FCR_RX_SOFT_RESET|UA5_FCR_TX_SOFT_RESET);

    c_printf("Turning Off Enable Bits...\n");
    __outb(UA4_INT_ENABLE_REG, 0);
    
    c_printf("Setting Baud Rate to 9600bps...\n");
    __outb(UA4_BANK_SELECT, UA4_LCR_BANK1);
    __outb(UA4_LBGD_L, BAUD_LOW_BYTE( BAUD_9600));
    __outb(UA4_LBGD_H, BAUD_HIGH_BYTE( BAUD_9600));
    
    c_printf("Setting Character Format in Bank 0...\n");
    __outb(UA4_BANK_SELECT, UA4_LCR_BANK0);
    __outb(UA4_LINE_CTL, UA4_LCR_BANK0 |UA4_LCR_BITS_8 | UA4_LCR_NO_PARITY | 
                            UA4_LCR_1_STOP_BIT);
    
    c_printf("Configuring Modem Control Register...\n");
    __outb(UA4_MODEM_CTL, UA4_MCR_INT_SIGNAL_ENABLE |
                          UA4_MCR_DATA_TERMINAL_READY | 
                          UA4_MCR_READY_TO_SEND);
    
    c_printf("Enabling Device Interrupts...\n");
    __outb(UA4_INT_ENABLE_REG, UA4_IER_RX_INT_ENABLE | UA4_IER_TX_INT_ENABLE
                               | UA4_IER_LS_IE | UA4_IER_MS_IE);
}

/**
 * Notifies any routines blocked for input that the serial
 * device is ready to be read from (i.e. it has data to be read)
 */
inline void sio_set_rx_ready(void)
{
    rx_ready_flag = 1;
}

/**
 * Notifies any routines blocked for input that the serial
 * device is ready to be written to (i.e. it is ready to receive data)
 */
inline void sio_set_tx_ready(void)
{
    tx_ready_flag = 1;
}


int sio_get_rx_state()
{
    return rx_ready_flag;
}

int sio_get_tx_state()
{
    return tx_ready_flag;
}

void read_in_char(char ch)
{
    if (read_enable_flag) //if we can read
    {
        input_buffer[next_char] = ch;
        next_char++;
        if(next_char >= INPUT_BUFFER_SIZE)
        {
            next_char = 0;
        }
    }
    
    int i;
    for(i=0; i< INPUT_BUFFER_SIZE; ++i)
    {
        c_putchar(input_buffer[i]);
    }
    c_putchar('\n');
}


void sio_set_echo_status(int echo_flag_new)
{
    echo_enable_flag = echo_flag_new;
}


void sio_interrupt(int vector, int code)
{
    int event;
    
    while (1)
    {
        event = __inb(UA4_EIR) & UA4_EIR_INT_PRI_MASK;

        if (event == UA4_EIR_NO_INT)              // no interrupt
        {
            //c_printf("No Interrupt!\n");
            break;
        }
        else if (event == UA4_EIR_LINE_STATUS)    // line status
        {
            c_printf("Line Status!\n");
            __inb(UA4_LINE_STATUS);
        }
        else if (event == UA4_EIR_RX_HIGH)        // high data level event
        {
       read_in_char(__inb(UA4_RX_DATA));
        }
        else if (event == UA5_EIR_RX_FIFO_TO)     // receiver FIFO Time-Out
        {
            c_printf("Timeout!\n");
            __inb(UA4_RX_DATA);
        }
        else if (event == UA4_EIR_TX_LOW)         // low data level event
        {
            //c_printf("Low Data!\n");
            //c_puts("out, ");
            sio_set_tx_ready();
            __inb(UA4_EVENT_ID);
        }
        else if (event == UA4_EIR_MODEM_STATUS)   // modem status interrupt
        {
            c_printf("Modem Status!\n");
            __inb(UA4_MODEM_STATUS);
        }
        else
        {
            c_printf("I am confused! Why %d?\n", event);
        }
    }
    __outb( PIC_MASTER_CMD_PORT, PIC_EOI );
}

void sio_putchar(char ch)
{
    
    if( ch == '\n' )
    {
        sio_putchar('\r');
    }
    
    while (!tx_ready_flag);// { c_printf("tryingWrite, "); }
    
    tx_ready_flag = 0;
    __outb (UA4_TXD, ch);
}

int sio_getchar()
{
    int old_read_enable = read_enable_flag;
    read_enable_flag = 1;
    
    while (buff_read == next_char); // wait for some difference in characters
    int ch = input_buffer[buff_read++];
    buff_read %= (INPUT_BUFFER_SIZE+1);

    
    if(buff_read == next_char) // if there are no more characters
        rx_ready_flag = 0;     // then there is nothing more to be read...
    
    if (ch == CTRL_D)
    {
        ch = EOF;
    }
    else
    {
        if (ch == '\r')
            ch = '\n';
        if (echo_enable_flag)
            sio_putchar(ch);
    }
    
    read_enable_flag = old_read_enable;
    return ch;
}

void sio_puts(char *str)
{
    while(*str != '\0')
    {
        sio_putchar(*str);
        str++;
    }
}

unsigned int sio_gets(char* buffer, size_t size)
{
    int old_read_enable = read_enable_flag;
    
    // start the bufer off at the beginning to discard previous contents
    next_char = 0;
    buff_read = 0;
    
    // allow registering of characters
    read_enable_flag = 1;
    
    char* bp = buffer;
    char ch;

    
    while (size > 1)
    {
        ch = sio_getchar();
        
        if (ch == EOF)
        {
            break;
        }
        
        *bp = ch;
        bp++;
        size--;
        
        if (ch == '\n') break;
    }
    
    *bp = '\0';
    
    read_enable_flag = old_read_enable;
    
    return bp - buffer;
}

void sio_print_integer( int value )
{
	int quotient = value / 10;
	if( quotient > 0 )
		sio_print_integer( quotient );
		
	sio_putchar(value % 10 + '0');
}

