
/**
 * Library to substitute those usually found in the C standard lib
 * and then some.
 *
 * Author: Daniel Rosenwasser, Matthew Newton
 */

#ifndef __UTILITY_H_
#define __UTILITY_H_

#include "types.h"

#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)

/**
 * @returns the length of the string, or the number of characters before
 * the NUL byte, or the position at which the NUL byte resides.
 */
size_t strlen(const char* str);

/**
 * Converts a null-terminated string to an integer, where the string is
 * assumed to have a regular-expression form of
 *
 *    -?[0-9].
 *
 * @returns the integer form of the given string representation
 */
int atoi(const char* str);

/**
 * @returns the absolute value of its argument as an unsigned integer
 */
unsigned int abs(int n);

/**
 * @returns the necessary length of a buffer for an integer before
 * passing it to itoa.
 */
int itoa_buff_length(int number);

/**
 * Reverses a string given a pointer to the first character as well
 * as a pointer to the last non-NUL character
 */
void str_reverse(char *begin, char *end);

/**
 * Converts a number into its string representation in the given buffer.
 *
 * The necessary buffer size can be determined by using itoa_buff_length
 */
void itoa(unsigned int number, char *buff);

#endif
