
/**
 * Author: Daniel Rosenwasser, Matthew Newton
 *
 * Description: library to substitute those usually found in the C standard lib
 *
 */

#include "utility.h"
#include "types.h"

size_t strlen(const char* str)
{
    size_t n = 0;
    
    while ( *(str++) != '\0' )
    {
        n++;
    }
    
    return n;
}

int atoi(const char* str)
{
    int result = 0;
    int sign = 1;

    // check if sign is negative
    if ( *str == '-' )
    {
        sign = -1;
        str++;
    }

    // until we hit the NUL byte, do a base-10 left shift
    // on the result and add the current digit encountered
    while ( *str != '\0' )
    {
        result *= 10;
        result += *str - '0';
    }
    
    return result * sign;
}

unsigned int abs(int n)
{
    if (n < 0) n *= -1;
    
    return n;
}


//Returns the length of a string 
//that will be produced by our itoa
//@params number - int to calculate string length for
int itoa_buff_length(int number)
{
    int len =0;    
    while(number){
        len++;
        number /= 10;
    }
    return len;
}

//Reverses a string
//@params begin - pointer to the begining of the string
//@params end - pointer to the end of the string (not including null)
void str_reverse(char *begin, char *end)
{    
    char temp;
    while(end>begin) {
        temp =* end;    /*store end*/
        *end-- =* begin;/*write begin to end, move end */
        *begin++ = temp;/*write temp(which holds end)*/ 
                /*to begin, move begin*/
    }
}

//Puts a string representation of number into buff
//number must be unsigned and buffer is assumed to be large enough!
//@params number - number to convert to string
//@params buffer - where to put the string
void itoa(unsigned int number, char *buff)
{
    static char num[] = "0123456789";
    char *curr=buff;
    while(number){
        *curr = num[number%10]; 
        number /= 10;
        curr ++;
    }
    //*curr='\0';
    str_reverse(buff, curr-1);
}

