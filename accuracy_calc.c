
/**
 *
 * Author:	Daniel Rosenwasser
 *
 */

#include "accuracy_calc.h"
#include "utility.h"
#include "c_io.h"

unsigned int differences(const char* str_a, const char* str_b)
{
    size_t length_a = strlen(str_a);
    size_t length_b = strlen(str_b);
    
    unsigned int num_diffs = abs(length_a - length_b);
    
    c_printf("Difference in size of %d", num_diffs);
    
    int i;
    for (i = 0; i < MIN(length_a, length_b); i++)
    {
        if (str_a[i] != str_b[i])
        {
            c_printf("At Index %d, expected %x, got %x\n", i, str_a[i], str_b[i]);
            num_diffs++;
        }
    }
    
    return num_diffs;
}

unsigned int chars_per_second(unsigned int nChars, unsigned int nSecs)
{
    return (nSecs != 0) ? nChars / nSecs : 0;
}

unsigned int tenths_chars_per_second(unsigned int nChars, unsigned int nSecs)
{
    return (nSecs != 0) ? ((10 * nChars) / nSecs) % 10 : 0;
}

