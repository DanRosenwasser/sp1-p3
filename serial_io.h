
/**
 * A basic IO library for sending and receiving bytes (characters)
 * to a serial device. Aims to emulate certain behavior as in the
 * C stdio library.
 *
 * Author: Daniel Rosenwasser, Matthew Newton
 */

#ifndef __SERIAL_IO_H
#define __SERIAL_IO_H

#define EOF -1

#include "types.h"

/**
 * Sends the appropriate messages to the device and configures it to have 
 */
void init_sio(void);

/**
 * Handles all queued interrupts in the serial IO device in the
 * appropriate order as specified by the documentation.
 */
void sio_interrupt(int vector, int code);

/**
 * Determines whether or not the serial IO device has sent back data
 *
 * @returns 0 if there is no data to be read; 1 if there is data to be read
 */
int sio_get_rx_state(void);

/**
 * Determines whether or not the serial IO device is waiting for data
 *
 * @returns 0 if the device isn't ready; 1 if the serial device is ready to read
 */
int sio_get_tx_state(void);

/**
 * Specifies whether or not sio_getchar and sio_gets will actually echo
 * out the characters which they receive to the serial device.
 *
 * @param echo_flag_new the state of the echo flag after this function call,
 *                      which should be 0 to disable echo and anything else
                        to enable echoing back to the device.
 */
void sio_set_echo_status(int echo_flag_new);

/**
 * Sends the character ch to the serial device. This function will block
 * until the device is ready to read in a character.
 * 
 * Translation from NL to CRNL is automatically done by this function.
 *
 * @param ch the character to send to the serial device
 */
void sio_putchar(char ch);

/**
 * Reads a character from the serial device. This function will block
 * until the device has a character to be read.
 * 
 * Translation from CR to NL is automatically done by this function.
 * The character read will be echoed back to the device using sio_putchar(char)
 *
 * @returns the character received from the device
 */
int sio_getchar(void);

/**
 * Sends the contents of a null-terminated string of characters to the device
 * All characters are sent in a linear order using sio_putchar(char)
 *
 * @param str the string to send the device
 */
void sio_puts(char *str);

/**
 * Reads in characters up until the first end-of-line, end-of-file, or until
 * 1 less than the size of the buffer. The buffer is then null-terminated.
 *
 * @param buffer the buffer to store the string which was read in
 * @param size the size of the buffer, which is 1 more than the max input size
 */
unsigned int sio_gets(char* buffer, size_t size);

/**
 * Prints an integer value on the serial I/O port, using sio_putchar
 * Taken from slide 12 of lecture notes 9
 */
void sio_print_integer(int value);

#endif


