
/**
 *
 * Author: Daniel Rosenwasser
 *
 */

#ifndef __ACCURACY_CALC
#define __ACCURACY_CALC

/**
 * Calculates the number of differences between two strings.
 *
 * The formula is given by the absolute value of the difference in length
 * added to the number of differences in characters at respective positions
 */
unsigned int differences(const char* str_a, const char* str_b);

/**
 * Given the number of characters entered and the number of seconds it took
 * to enter them, returns the whole number characters per second.
 *
 * If nSecs is 0, so is the result.
 */ 
unsigned int chars_per_second(unsigned int nChars, unsigned int nSecs);

/**
 * Given the number of characters entered and the number of seconds it took
 * to enter them, returns the number of tenths of characters per second while
 * discarding the whole number portion.
 *
 * If nSecs is 0, so is the result.
 */
unsigned int tenths_chars_per_second(unsigned int nChars, unsigned int nSecs);

#endif
